<?php

namespace HalcyonLaravelBoilerplate\Permalink\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * HalcyonLaravelBoilerplate\Permalink\Models\Redirect
 *
 * @property int $id
 * @property string|null $model_type
 * @property int|null $model_id
 * @property string $old_url
 * @property string|null $new_url
 * @property int $status_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|Redirect newModelQuery()
 * @method static Builder|Redirect newQuery()
 * @method static Builder|Redirect query()
 * @method static Builder|Redirect whereCreatedAt($value)
 * @method static Builder|Redirect whereId($value)
 * @method static Builder|Redirect whereModelId($value)
 * @method static Builder|Redirect whereModelType($value)
 * @method static Builder|Redirect whereNewUrl($value)
 * @method static Builder|Redirect whereOldUrl($value)
 * @method static Builder|Redirect whereStatusCode($value)
 * @method static Builder|Redirect whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Redirect extends Model
{
    protected $fillable = [
        'model_type',
        'model_id',
        'old_url',
        'new_url',
        'status_code',
    ];
}
