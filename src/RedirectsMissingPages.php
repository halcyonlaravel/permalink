<?php

namespace HalcyonLaravelBoilerplate\Permalink;

use HalcyonLaravelBoilerplate\Permalink\Models\Redirect;
use Spatie\MissingPageRedirector\Redirector\Redirector;
use Symfony\Component\HttpFoundation\Request;

class RedirectsMissingPages implements Redirector
{
    public function getRedirectsFor(Request $request): array
    {
        $oldUrl = urldecode($request->path());
        $redirect = app(Redirect::class)->where('old_url', $oldUrl)->first();
        $configRedirect = config('missing-page-redirector.redirects');

        if (blank($redirect)) {
            return $configRedirect;
        }

        if (blank($redirect->new_url) && !blank($redirect->model_type) && !blank($redirect->model_id)) {
            $model = app($redirect->model_type)->where('id', $redirect->model_id)->first();

//            dd(__METHOD__,$model);
            if (blank($model)) {
                return $configRedirect;
            }

            return [
                $oldUrl => [
                    $model->domain_url,// TODO: generate url
                    $redirect->status,
                ]
            ];
        } elseif (!blank($redirect->new_url)) {
            return [
                $oldUrl => [
                    $redirect->new_url,
                    $redirect->status,
                ]
            ];
        }

        return $configRedirect;
    }
}
