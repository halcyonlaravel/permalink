# Release Notes for 2.x

    2021-04-23 v2.0.1: Add support for PHP8, use composer v2 in dev
    2020-03-10 v2.0.0: Prepare for Laravel 7